# Danish Windows Keyboard on MacOS
- maps programing keyboard keys ( { } | \ ) to the correct positions
- Created with https://software.sil.org/ukelele/ (version 3.5.4)

## Installation

### Copy bundle to keyboard layouts
```sh
cp DALK2.bundle ~/Library/Keyboard\ Layouts/
```

### Open 'System Preferences'
 -> Keyboard
 -> input source
 -> add the keyboard layout (check Danish) 

### Possible Problem With Swapped Keys
- the "<>" key may on some keyboards with swapped with the "$§" key

You will then need to run EACH time your computer restarts. 

```sh
remap-keyboard-mac.command      # see attached file 
```

this can be fixed by adding the script to your startup items

### Open 'System Preferences'
-> Users & Groups
-> Login Items
-> add "remap-keyboard-mac.command" 


## Test
Appears to work on MacOS 12, 10.15.7


## keywords:
Danish Windows Keyboard on Mac
Keyboard Layouts
ukelele

## Displaimer: Use your own risk, I am!
updated 2021-12-09