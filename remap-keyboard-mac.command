#!/bin/bash

# https://developer.apple.com/library/archive/technotes/tn2450/_index.html
# https://www.nanoant.com/mac/macos-function-key-remapping-with-hidutil
# https://dchakarov.com/blog/macbook-remap-keys/
# https://apple.stackexchange.com/questions/329085/tilde-and-plus-minus-%C2%B1-in-wrong-place-on-keyboard

	
echo "version 2 /LK 2020-09-25"
echo "- swap key '$ §' with'< >' to correct position on keyboard"
echo "- replace nummeric key ',' with '.' "


# reset mapping
hidutil property --set '{"UserKeyMapping":[]}'

# new Mapping
hidutil property --set '{"UserKeyMapping":[
{"HIDKeyboardModifierMappingSrc":0x700000035,"HIDKeyboardModifierMappingDst":0x700000064},
{"HIDKeyboardModifierMappingSrc":0x700000064,"HIDKeyboardModifierMappingDst":0x700000035},
{"HIDKeyboardModifierMappingSrc":0x700000063,"HIDKeyboardModifierMappingDst":0x700000037}
]}'

 
 
 